window.microsoftTeams.initialize();

function authenticate(e, urlToOpen){
    var url = urlToOpen || 'https://www.bentley.com'

    window.microsoftTeams.authentication.authenticate({
        url: url,
        height: 600,
        width: 400,
        successCallback: function(){
            console.log('the call was successful')
        },
        failureCallback: function(e){
            console.error(e)
            console.log('the call was not successful:', e)
        },
      })
}

function openWindow(e, urlToOpen){
    var url = urlToOpen || 'https://www.bentley.com'
    window.open(url, '', 'width=400,height=600')
}

document.getElementById("authenticate").onclick = authenticate
document.getElementById("openWindow").onclick = openWindow